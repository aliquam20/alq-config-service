create table public.config
(
    key      varchar(255)  not null primary key,
    value    varchar(1023) not null,
    internal bit           not null
);
