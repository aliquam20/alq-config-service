create table public.config
(
    key      varchar(255) not null primary key,
    value    text         not null,
    internal boolean      not null
);

insert into public.config("key", "value", "internal")
VALUES ('ping', '"pong"', false),
       ('the-answer', '42', true),
       ('alq-gateway.hosts', '[
         {
           "name": "host_example",
           "target": "host.docker.internal:8080"
         },
         {
           "name": "perms",
           "target": "alq-perms-service:8080"
         },
         {
           "name": "players",
           "target": "alq-players-service:8080"
         },
         {
           "name": "worlds",
           "target": "alq-worlds-service:8080"
         },
         {
           "name": "chat",
           "target": "alq-chat-service:8080"
         },
         {
           "name": "frontend",
           "target": "alq-frontend:8080"
         }
       ]', false),
       ('is castro12321 handsome', 'true', false);
