package org.aliquam.config.service.model;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "config")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class ConfigModel {

    @Id
    private String key;

    @Column(name = "value")
    private String raw;

    private boolean internal;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ConfigModel that = (ConfigModel) o;
        return key != null && Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
