package org.aliquam.config.service.mapper;

import org.aliquam.config.api.model.Config;
import org.aliquam.config.service.model.ConfigModel;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper
public interface ConfigMapper {

    @Mapping(target = "value", ignore = true)
    Config map(ConfigModel configModel);

    List<Config> map(Iterable<ConfigModel> configModels);

    ConfigModel map(Config configModel);

    ConfigModel map(Config configModel, @MappingTarget ConfigModel target);

}
