package org.aliquam.config.service.controller;

import lombok.extern.slf4j.Slf4j;
import org.aliquam.config.api.model.Config;
import org.aliquam.config.service.mapper.ConfigMapper;
import org.aliquam.config.service.model.ConfigModel;
import org.aliquam.config.service.repository.ConfigRepository;
import org.aliquam.cum4service.session.RequireSession;
import org.aliquam.cum4service.session.RequireSessionAspect;
import org.aliquam.session.api.AlqSessionApi;
import org.aliquam.session.api.RequestAuthenticatorService;
import org.aliquam.session.api.connector.AlqSessionConnector;
import org.aliquam.session.api.connector.AlqSessionManager;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@Slf4j
public class ConfigController {
    private final ConfigRepository configRepository;
    private final ConfigMapper configMapper;
    private final RequestAuthenticatorService authenticatorService;

    public ConfigController(ConfigRepository configRepository, ConfigMapper configMapper, AlqSessionManager alqSessionManager) {
        this.configRepository = configRepository;
        this.configMapper = configMapper;
        AlqSessionConnector sessionConnector = new AlqSessionConnector(alqSessionManager);
        authenticatorService = new RequestAuthenticatorService(sessionConnector::getSessionOrEmpty);
    }

    @GetMapping("/config")
    @RequireSession(internal = true)
    public List<Config> listConfigs() {
        Iterable<ConfigModel> configModels = configRepository.findAll();
        return configMapper.map(configModels);
    }

    @GetMapping("/config/{key}")
    @RequireSession(internal = false)
    public Config getConfig(@RequestHeader(AlqSessionApi.SESSION_HEADER) String sessionHeader,
                            @PathVariable @NotNull String key) {
        log.info(">> getConfig({})", key);
        ConfigModel configModel = configRepository.findById(key)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Config does not exist for key " + key));
        if(configModel.isInternal()) {
            RequireSessionAspect.checkSession(authenticatorService, sessionHeader, true);
        }
        Config ret = configMapper.map(configModel);
        log.info("<< {}", ret);
        return ret;
    }

    @PutMapping("/config/{key}")
    @RequireSession(internal = true)
    @Transactional
    public void setConfig(@PathVariable @NotNull String key,
                          @RequestBody @Valid Config config) {
        if(!key.equals(config.getKey())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Config-key and key-provided-in-path-variable mismatch");
        }

        ConfigModel configModel = configRepository.findById(key)
                .map(existing -> configMapper.map(config, existing))
                .orElseGet(() -> configMapper.map(config));
        configRepository.save(configModel);
    }

    @DeleteMapping("/config/{key}")
    @RequireSession(internal = true)
    @Transactional
    public void deleteConfig(@PathVariable @NotNull String key) {
        if (configRepository.existsById(key)) {
            configRepository.deleteById(key);
        }
    }

}
