package org.aliquam.config.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(org.aliquam.cum4service.SpringBootConfigurationReference.class)
public class AlqConfigServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlqConfigServiceApplication.class, args);
	}

}
