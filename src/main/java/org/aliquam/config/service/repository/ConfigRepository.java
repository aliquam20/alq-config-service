package org.aliquam.config.service.repository;

import org.aliquam.config.service.model.ConfigModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//@RepositoryRestResource(collectionResourceRel = "config", path = "config") --> Removed because...
// ...I don't know how to use @RequireSession with this
public interface ConfigRepository extends CrudRepository<ConfigModel, String> {

}
